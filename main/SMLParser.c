//#include <esp_wifi.h>
//#include "mqtt_client.h"
#include <stdlib.h>
#include "SMLParser.h"
#include <stdio.h>
static const char *PARSER_TAG = "SML_PARSER";
unsigned int DATA_LENGTH;

typedef struct OBIS
{
    char *name;
    uint8_t value[OBIS_LENGTH];
    unsigned int data_length;
    unsigned int offset; // how many bytes between the last OBIS byte and the first occuring byte of the parameter value
    uint8_t *bytes;
    float p_value;
} obis;

obis obis_list[NUM_PARAMS] = {
    {"Hersteller-Kennung", {129, 129, 199, 130, 3, 255}, 3, 5},
    {"Geräte ID", {1, 0, 0, 0, 9, 255}, 10, 5},
    {"Zählwerk pos Wirkenergie Tariflos", {1, 0, 1, 8, 0, 255}, 5, 10},
    {"Zählwerk pos Tarif 1", {1, 0, 1, 8, 1, 255}, 5, 7},
    {"Zählwerk pos Tarif 2", {1, 0, 1, 8, 2, 255}, 5, 7},
    {"Zählwerk neg Wirkenergie Tariflos", {1, 0, 2, 8, 0, 255}, 5, 10},
    {"Zählwerk neg Tarif 1", {1, 0, 2, 8, 1, 255}, 5, 7},
    {"Zählwerk neg Tarif 2", {1, 0, 2, 8, 2, 255}, 5, 7},
    {"Aktuelle Wirkleistung", {1, 0, 16, 7, 0, 255}, 4, 7},
    {"EDL40 Zählerstand", {1, 0, 1, 17, 0, 255}, 14}, // To be ignored!
    {"Public Key", {129, 129, 199, 130, 5, 255}, 48, 6}};

int parse_data(uint8_t *data, int n, char** json, int* data_len, float* values)
{
    char packet_json[350];
    DATA_LENGTH = n;
    printf("data_length: %d", DATA_LENGTH);
    uint32_t wirkleistung = 0;  
    float params[6];
    //float *pParams = (float*) &params;
    for (int i = 0; i < DATA_LENGTH - (OBIS_LENGTH); i++)
    {
        uint8_t next_bytes[OBIS_LENGTH];
        for (int k = 0; k < OBIS_LENGTH; k++)
        {
            next_bytes[k] = data[i + k];
        }
        for (int j = 0; j < NUM_PARAMS; j++)
        {
            if (are_arrays_equal(next_bytes, obis_list[j].value, OBIS_LENGTH, OBIS_LENGTH))
            {
                obis_list[j].bytes = (uint8_t*)malloc(sizeof(uint8_t) * obis_list[j].data_length);
                if (!obis_list[j].bytes)
                {
                    //ESP_LOGE(PARSER_TAG, "couldn't allocate memory for %s \n", obis_list[i].name);
                    printf("couldn't allocate memory for %s \n", obis_list[j].name);
                    return 1;
                }
                int l = 0;
                int index = i + OBIS_LENGTH + obis_list[j].offset;
                while( l < obis_list[j].data_length)
                {
                    obis_list[j].bytes[l] = data[index+l];
                    l++;
                }
                if(j>=2 && j<=7) //pos&neg Wirkenergie
                {
                    //printf("got j>=2 && j<=7");

                    uint64_t value_energy = obis_list[j].bytes[4] |
                                            (uint64_t)obis_list[j].bytes[3] << 8 |
                                            (uint64_t)obis_list[j].bytes[2] << 16 |
                                            (uint64_t)obis_list[j].bytes[1] << 24 |
                                            (uint64_t)obis_list[j].bytes[0] << 32 ;
                    float val_flt =  (float) value_energy / 10000.0f;                       
                }    
                if (j == 8) // aktuelle Wirkleistung
                {
                    uint32_t value_power = (obis_list[j].bytes[3]) | ((uint32_t)obis_list[j].bytes[2] << 8) | ((uint32_t)obis_list[j].bytes[1]) << 16 | ((uint32_t)obis_list[j].bytes[0] << 24);
                    printf("%s:  %d W \n", obis_list[j].name, value_power / 10);
                }
          
            }
        }
    }
    wirkleistung = (obis_list[8].bytes[3] | (uint32_t)obis_list[8].bytes[2] << 8 | 
                   (uint32_t)obis_list[8].bytes[1] << 16 | (uint32_t)obis_list[8].bytes[0] << 24)/10;
    for(int j=2; j<=7; j++)
    {
        uint64_t value_energy = obis_list[j].bytes[4] |
                                (uint64_t)obis_list[j].bytes[3] << 8 |
                                (uint64_t)obis_list[j].bytes[2] << 16 |
                                (uint64_t)obis_list[j].bytes[1] << 24 |
                                (uint64_t)obis_list[j].bytes[0] << 32 ;
        float val_flt =  (float) value_energy / 10000.0f; 
        params[j-2] = val_flt;
    }
    sprintf(packet_json, "{Pos Wirkenergie Tariflos: %.4f, Pos Tarif 1: %.4f, Pos Tarif 2: %.4f, Neg Wirkenergie Tariglos: %.4f, Neg Tarif 1: %.4f, Neg Tarif 2: %.4f, Aktuelle Wirkleistung: %d}", params[0], params[1], params[2],params[3],params[4],params[5], wirkleistung);
    *json = (char *)malloc(strlen(packet_json)+1);
    strcpy(*json,packet_json); 
    printf(packet_json);
    printf("json: %s\n",*json);
    for(int i=0; i<6; i++)
    {
        *values = params[i];
        values++;
    }
    *values=wirkleistung;
    *data_len = strlen(packet_json)+1;
    garbage_collect();
  
    return 0;
}
void garbage_collect()
{
    for (int i = 0; i < NUM_PARAMS; i++)
    {
        free(obis_list[i].bytes);
    }
}

// -------- Helpers --------
int are_arrays_equal(uint8_t arr1[], uint8_t arr2[], char size_arr1, char size_arr2)
{
    if (size_arr1 != size_arr2)
    {
        return 0;
    }
    else
    {
        for (int i = 0; i < size_arr1; i++)
        {
            if (arr1[i] != arr2[i])
                return 0;
        }
    }
    return 1;
}

uint32_t to_int(uint8_t bytes[4], int length)
{
    //for (int i=0; i<length; i++)
    //{
    //value |= bytes[i] << 4^i;
    //}
    uint32_t value = (bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8 | bytes[3]);
    return value;
}
// size_t ReadHexFile(FILE *inf, unsigned char *dest)
// {
//   size_t count = 0;
//   int n;
//   if (dest == NULL) {
//     unsigned char OneByte;
//     while ((n = fscanf(inf, "%hhx", &OneByte)) == 1 ) {
//       count++;
//     }
//   }
//   else {
//     while ((n = fscanf(inf, "%hhx", dest)) == 1 ) {
//       dest++;
//     }
//   }
//   if (n != -1) {
//     ;  // handle syntax error
//   }
//   return count;
// }
