#include "esp_http_server.h"
#include "html_charts.c"
#include "html_main.c"
#include "html_mqtt.c"
#include "html_mqtt_success.c"
#include "html_mqtt_failure.c"
#include "mqtt_client.h"
#include <esp_err.h>
#include <esp_log.h>


esp_err_t post_form_handler(httpd_req_t *req);
esp_err_t get_handler(httpd_req_t *req);
esp_err_t get_charts_handler(httpd_req_t *req);
esp_err_t get_mqtt_handler(httpd_req_t *req);
esp_err_t get_plan_handler(httpd_req_t *req);
esp_err_t get_wirkleistung_handler(httpd_req_t *req);
httpd_handle_t start_webserver(void);
void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data);
void stop_webserver(httpd_handle_t server);
int mqtt_init(void);
typedef struct MQTT_Data
{
    char hostname[40];
    char port[10];
    char username[10];
    char password[15];
    char pub_topic[25];
    char sub_topic[25];
} MQTT;

