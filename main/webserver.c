#include "webserver.h"
#include "helpers.c"
extern int plot_interval;
extern float values[6];
extern esp_mqtt_client_handle_t client;
static const char *TAG_MQTT = "MQTT";
extern char* status;
extern MQTT *mqtt_data;
httpd_uri_t uri_get_mainpage = {
    .uri = "/",
    .method = HTTP_GET,
    .handler = get_handler,
    .user_ctx = NULL};
httpd_uri_t uri_get_wirkleistung = {
    .uri = "/wirkleistung",
    .method = HTTP_GET,
    .handler = get_wirkleistung_handler,
    .user_ctx = NULL};
httpd_uri_t uri_get_plan = {
    .uri = "/get_plan",
    .method = HTTP_GET,
    .handler = get_plan_handler,
    .user_ctx = NULL};
httpd_uri_t uri_post_form = {
    .uri = "/form",
    .method = HTTP_GET,
    .handler = post_form_handler,
    .user_ctx = NULL};
httpd_uri_t uri_charts = {
    .uri = "/charts",
    .method = HTTP_GET,
    .handler = get_charts_handler,
    .user_ctx = NULL};
httpd_uri_t uri_mqtt = {
    .uri = "/mqtt",
    .method = HTTP_GET,
    .handler = get_mqtt_handler,
    .user_ctx = NULL};
// //................................................ Server..............................................................//
static void log_error_if_nonzero(const char* tag, const char *message, int error_code)
{
    if (error_code != 0)
    {
        ESP_LOGE(tag, "Last error %s: 0x%x", message, error_code);
    }
}
esp_err_t post_form_handler(httpd_req_t *req)
{
    char *buf;
    char buffer_result[100];
    size_t buf_len = httpd_req_get_url_query_len(req) + 1;
    if (buf_len > 1)
    {
        //mqtt_data = (MQTT *)malloc(sizeof(MQTT));
        buf = malloc(buf_len);
        if (buf != NULL)
        {
            ESP_ERROR_CHECK(httpd_req_get_url_query_str(req, buf, buf_len));
            ESP_ERROR_CHECK(httpd_query_key_value(buf, "host", mqtt_data->hostname, sizeof(mqtt_data->hostname)));
            ESP_ERROR_CHECK(httpd_query_key_value(buf, "port", mqtt_data->port, sizeof(mqtt_data->port)));
            ESP_ERROR_CHECK(httpd_query_key_value(buf, "username", mqtt_data->username, sizeof(mqtt_data->username)));
            ESP_ERROR_CHECK(httpd_query_key_value(buf, "password", mqtt_data->password, sizeof(mqtt_data->password)));
            ESP_ERROR_CHECK(httpd_query_key_value(buf, "pub_topic", mqtt_data->pub_topic, sizeof(mqtt_data->pub_topic)));
            ESP_ERROR_CHECK(httpd_query_key_value(buf, "sub_topic", mqtt_data->sub_topic, sizeof(mqtt_data->sub_topic)));
            
            str_replace(mqtt_data->hostname, "%3A%2F%2F", "://"); // decode url //
            free(buf);
            buf = NULL;

        }

        if (mqtt_init() == 0)
        {
            snprintf(buffer_result, sizeof(buffer_result), mqtt_success_html);

        }
        else
        {
            snprintf(buffer_result, sizeof(buffer_result), mqtt_fail_html, mqtt_data->hostname);
        }

        httpd_resp_send(req, buffer_result, HTTPD_RESP_USE_STRLEN);
        //free(mqtt_data);
        //mqtt_data = NULL;
    }
    return ESP_OK;
}

esp_err_t get_handler(httpd_req_t *req)
{
    char buffer[3000];
    snprintf(buffer, sizeof(buffer), main_html, status);
    httpd_resp_send(req, buffer, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

esp_err_t get_charts_handler(httpd_req_t *req)
{
    char buffer[10000];
    snprintf(buffer, sizeof(buffer), pre_charts, values[0], values[1], values[2], values[3], values[4], plot_interval);
    httpd_resp_send(req, buffer, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}
esp_err_t get_mqtt_handler(httpd_req_t *req)
{
    uint32_t port = 2;
    char buffer[4000];
    snprintf(buffer, sizeof(buffer), mqtt_html, "0", port, "0", "0", "0", "0");
    httpd_resp_send(req, buffer, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}
esp_err_t get_plan_handler(httpd_req_t *req)
{
    int interval = 20000;
    char *buf;
    size_t buf_len;
    buf_len = httpd_req_get_url_query_len(req) + 1;
    if (buf_len > 1)
    {
        buf = malloc(buf_len);
        httpd_req_get_url_query_str(req, buf, buf_len);
        char param[32];
        if (httpd_query_key_value(buf, "plan", param, sizeof(param)) == ESP_OK)
        {
            if (strcmp(param, "default") == 0)
            {
                interval = 1000;
            }
            else if (strcmp(param, "opm") == 0)
            {
                interval = 60 * 1000;
            }
            else if (strcmp(param, "opd") == 0)
            {
                interval = 24 * 60 * 60 * 1000;
            }
            else if (strcmp(param, "opw") == 0)
            {
                interval = 7 * 24 * 60 * 60 * 1000;
            }
            else if (strcmp(param, "oph") == 0)
            {
                interval = 60 * 60 * 1000;
            }
        }
    }
    plot_interval = interval;
    char buffer[10000];
    snprintf(buffer, sizeof(buffer), charts_html, values[0], values[1], values[2], values[3], values[4], plot_interval);

    httpd_resp_send(req, buffer, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

esp_err_t get_wirkleistung_handler(httpd_req_t *req)
{
    char buffer[20];
    snprintf(buffer, sizeof(buffer), "%f", values[6]);
    httpd_resp_send(req, buffer, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

httpd_handle_t start_webserver(void)
{
    /* Generate default configuration */
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.stack_size = 6 * 4096;
    /* Empty handle to esp_http_server */
    httpd_handle_t server = NULL;

    /* Start the httpd server */
    if (httpd_start(&server, &config) == ESP_OK)
    {
        /* Register URI handlers */
        httpd_register_uri_handler(server, &uri_get_mainpage);
        httpd_register_uri_handler(server, &uri_get_wirkleistung);
        httpd_register_uri_handler(server, &uri_get_plan);
        httpd_register_uri_handler(server, &uri_post_form);
        httpd_register_uri_handler(server, &uri_charts);
        httpd_register_uri_handler(server, &uri_mqtt);
    }
    /* If server failed to start, handle will be NULL */
    return server;
}

void stop_webserver(httpd_handle_t server)
{
    httpd_stop(server);
}
int mqtt_init(void)
{
    if (mqtt_data != NULL)
    {
        uint32_t port;
        sscanf(mqtt_data->port, "%d", &port);
        const esp_mqtt_client_config_t mqtt_cfg = {
            .uri = mqtt_data->hostname,
            .port = port,
            .username = mqtt_data->username,

        };
        ESP_LOGI("MQTT", "hostame: %s", mqtt_data->hostname);
        ESP_LOGI("MQTT", "port: %s", mqtt_data->port);


        client = esp_mqtt_client_init(&mqtt_cfg);
        if (client != NULL)
        {
            esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);

            if(esp_mqtt_client_start(client)==0)
            {
                return 0;
            }
            else
            {
                return -1;
            }
            
        }
    }

    return -1;
}
void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    ESP_LOGD(TAG_MQTT, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;
    switch ((esp_mqtt_event_id_t)event_id)
    {
    case MQTT_EVENT_CONNECTED:
        ESP_LOGI(TAG_MQTT, "MQTT_EVENT_CONNECTED");
        msg_id = esp_mqtt_client_subscribe(client, "/data/sub", 0);
        ESP_LOGI(TAG_MQTT, "sent subscribe successful, msg_id=%d", msg_id);
        break;
    case MQTT_EVENT_DISCONNECTED:
        ESP_LOGI(TAG_MQTT, "MQTT_EVENT_DISCONNECTED");
        break;

    case MQTT_EVENT_SUBSCRIBED:
        ESP_LOGI(TAG_MQTT, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
        msg_id = esp_mqtt_client_publish(client, mqtt_data->pub_topic, "Hellow world", 0, 0, 0);
        ESP_LOGI(TAG_MQTT, "sent publish successful, msg_id=%d", msg_id);
        break;
    case MQTT_EVENT_UNSUBSCRIBED:
        ESP_LOGI(TAG_MQTT, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_PUBLISHED:
        ESP_LOGI(TAG_MQTT, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_DATA:
        ESP_LOGI(TAG_MQTT, "MQTT_EVENT_DATA");
        printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
        printf("DATA=%.*s\r\n", event->data_len, event->data);
        break;
    case MQTT_EVENT_ERROR:
        ESP_LOGI(TAG_MQTT, "MQTT_EVENT_ERROR");
        if (event->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT)
        {
            log_error_if_nonzero(TAG_MQTT,"reported from esp-tls", event->error_handle->esp_tls_last_esp_err);
            log_error_if_nonzero(TAG_MQTT,"reported from tls stack", event->error_handle->esp_tls_stack_err);
            log_error_if_nonzero(TAG_MQTT,"captured as transport's socket errno", event->error_handle->esp_transport_sock_errno);
            ESP_LOGI(TAG_MQTT, "Last errno string (%s)", strerror(event->error_handle->esp_transport_sock_errno));
        }
        break;
    default:
        ESP_LOGI(TAG_MQTT, "Other event id:%d", event->event_id);
        break;
    }
}


