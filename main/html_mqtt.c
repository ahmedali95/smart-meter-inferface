  char* mqtt_html = "<!DOCTYPE html><html>"
    "<head>"
      "<title> MQTT Configurations</title> <meta charset=\"utf-8\""
      "name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\"> <link rel=\"icon\" href=\"data:,\">"  

      "<style>"
        "html{font-family: Helvetica; font-size: 90%%;}input[type=text], select{width: 100%%; padding: 7px; border: 1px"
        "solid #ccc; border-radius: 4px; resize: vertical; margin: 0px 0px 5px 0px; box-sizing: border-box;}input[type=submit]"
        "{background-color: #4CAF50; border: none; color: white; padding: 16px 32px; text-decoration: none; margin: 4px 2px;"
        "cursor: pointer; width: 100%%;}fieldset{border: 2px solid; border-radius: 3px; padding: 7px 7px 7px 7px; margin: 7px 0px;"
        "}.ext{border-color: #F00;}.container{background-color: #f2f2f2; padding: 10px;}.col{float: left; margin-top: 6px;}."
        "row:after{content: \"\"; display: table; clear: both;}"
      "</style>"

     "</head>"
     "<body> "
        "<form action=\"/form\"  method=\"get\" accept-charset=\"utf-8\" > <fieldset> <legend>MQTT:</legend> <fieldset class=\"ext\">" 
        "<legend>Iot Agent</legend> Host:<br><input type=\"text\" name=\"host\" value=\"%s\"><br>Port:<br><input type=\"text\" name=\"port\" value=\"%d\"> <br>Username:<br><input type=\"text\" name=\"username\" value=\"%s\"> <br>Password:<br><input type=\"text\" name=\"password\" value=%s><br> </fieldset> <fieldset class=\"ext\"> <legend>Topics:</legend>" 
        "Publish:<br><input type=\"text\" name=\"pub_topic\" value=\"%s\"><br>Subscribe:<br><input type=\"text\" name=\"sub_topic\""
        "value=\"%s\"> </fieldset></fieldset> <input type=\"submit\" value=\"Submit\"> </form>"
     "</body>"

     "</html>";