#define LEN_CURRENT_POWER 4
#define LEN_POS_ENERGY 4
#define LEN_POS_TARIFF1 4
#define LEN_POS_TARIFF2 4
#define LEN_NEG_ENERGY 4
#define LEN_NEG_TARIFF1 4
#define LEN_NEG_TARIFF2 4
#define LEN_AKTUELLE_WIRKLEISTUNG 4
#define LEN_ZAEHLER_STAND 4
#define LEN_PUB_KEY 4
#define OBIS_LENGTH  6
#define NUM_PARAMS  11
#include<stdint.h>
#include <stdio.h>
#include <string.h>

int parse_data(uint8_t *data, int n, char** json, int* data_len, float* values);
int are_arrays_equal(uint8_t arr1[], uint8_t arr2[], char size_arr1, char size_arr2);
int get_arr_size(char data[]);
uint32_t to_int(uint8_t bytes[], int length);
void garbage_collect();
//size_t ReadHexFile(FILE *inf, unsigned char *dest);
