#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "freertos/event_groups.h"
#include <esp_wifi.h>
#include <esp_system.h>
#include <nvs_flash.h>
#include <esp_log.h>
#include <string.h>
#include <esp_err.h>
#include "driver/uart.h"
#include "driver/gpio.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "../soc/esp32/include/soc/uart_reg.h"
#include "esp_intr_alloc.h"
#include "SMLParser.c"
#include "freertos/semphr.h"
#include "webserver.c"
#define EXAMPLE_ESP_WIFI_SSID "AndroidAPD252"
#define EXAMPLE_ESP_WIFI_PASS "12345galal"
#define EXAMPLE_ESP_MAXIMUM_RETRY 5
#define UART_PORT_NUM 0
#define UART_PIN_TXD 9
#define UART_PIN_RXD 10
#define BUFF_SIZE 385
#define BUF_SIZE (385)
#define RD_BUF_SIZE 385
static void mqtt_task(char *data_received, int data_len);
static QueueHandle_t uart1_queue;
uint8_t UART_RECEIVED = 0;
uint8_t notify = 0;
uint16_t counterISR = 0;
const int WIFI_CONNECTED_BIT = BIT0;
static int s_retry_num = 0;
uint8_t data[BUFF_SIZE] = {0};
static const char *TAG_UART = "UART";
static const char *TAG_WIFI = "WIFI";

static EventGroupHandle_t s_wifi_event_group;
esp_mqtt_client_handle_t client;
static TaskHandle_t mqtt_handle;
static TaskHandle_t uart_handle;
uint16_t length = 0;
int plot_interval = 4000;
char* status = "Idle";
static TaskHandle_t send_task_handle = NULL;
static TaskHandle_t receive_task_handle = NULL;
int intr_alloc_flags = ESP_INTR_FLAG_IRAM;
//static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data);
static void wifi_event_handler(void *handler_args, esp_event_base_t event_base, int32_t event_id, void *event_data);

float values[6];
MQTT mqtt_data_struct;
MQTT *mqtt_data=&mqtt_data_struct;
//.................................................... Init ..............................................................//

void wifi_init_sta(void)
{
    ESP_LOGI(TAG_WIFI, "ESP_WIFI_MODE_STA");

    s_wifi_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init());

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &wifi_event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &wifi_event_handler, NULL));

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = EXAMPLE_ESP_WIFI_SSID,
            .password = EXAMPLE_ESP_WIFI_PASS},
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    ESP_LOGI(TAG_WIFI, "wifi_init_sta finished.");
    ESP_LOGI(TAG_WIFI, "connect to ap SSID:%s password:%s",
             EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
}

static void uart_init()
{
    printf("inside uart init");
    const uart_port_t uart_num = UART_NUM_0;
    uart_config_t uart_config = {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
    };

    ESP_ERROR_CHECK(uart_param_config(uart_num, &uart_config));
    ESP_ERROR_CHECK(uart_set_pin(uart_num, 1, 3, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE));
    ESP_ERROR_CHECK(uart_driver_install(uart_num, BUFF_SIZE * 2, BUFF_SIZE * 2, 20, &uart1_queue, 0));
}
//................................................ Event Handlers ..............................................................//


static void wifi_event_handler(void *handler_args, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        if (s_retry_num < EXAMPLE_ESP_MAXIMUM_RETRY)
        {
            esp_wifi_connect();
            xEventGroupClearBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
            s_retry_num++;
            ESP_LOGI(TAG_WIFI, "retry to connect to the AP");
        }
        ESP_LOGI(TAG_WIFI, "connect to the AP fail");
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
        ESP_LOGI(TAG_WIFI, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}

static void uart_event_task()
{
    uart_event_t event;
    size_t buffered_size;
    uint8_t *dtmp = (uint8_t *)malloc(RD_BUF_SIZE);
    int ring_buff_len = 0;
    int len = 0;
    char *json_data;
    int notification_value;
    int data_len=0;
    uart_flush(UART_NUM_0);
    // Waiting for UART event.
    // notification_value = ulTaskNotifyTake(pdTRUE, (TickType_t)portMAX_DELAY);
    // if (notification_value > 0)
    //{
    for (;;)
    {
        vTaskDelay(300 / portTICK_PERIOD_MS); // wait
        if (xQueueReceive(uart1_queue, (void *)&event, (portTickType)portMAX_DELAY))
        {
            bzero(dtmp, RD_BUF_SIZE);
            ESP_LOGI(TAG_UART, "uart[%d] event:", UART_NUM_0);
            switch (event.type)
            {
            case UART_DATA:
                ESP_LOGI(TAG_UART, "[UART DATA]: %d", event.size);

                uart_get_buffered_data_len(UART_NUM_0, (size_t *)&ring_buff_len);
                printf("buffered_data_len: %d\n", ring_buff_len);
                if (ring_buff_len >= RD_BUF_SIZE)
                {
                    len = uart_read_bytes(UART_NUM_0, dtmp, ring_buff_len, 0);
                    printf("Received full packet of length %d \n Parsing ... \n", len);
                    status = "Running";
                    parse_data(dtmp, len, &json_data, &data_len, values);
                    mqtt_task(json_data, data_len);
                    uart_flush(UART_NUM_0);
                    free(json_data);
                    len = 0;
                    ring_buff_len = 0;
                    status = "Idle";
                }

                break;
            // Event of HW FIFO overflow detected
            case UART_FIFO_OVF:
                ESP_LOGI(TAG_UART, "hw fifo overflow");
                // If fifo overflow happened, you should consider adding flow control for your application.
                // The ISR has already reset the rx FIFO,
                // As an example, we directly flush the rx buffer here in order to read more data.
                uart_flush_input(UART_NUM_0);
                xQueueReset(uart1_queue);
                break;
            // Event of UART ring buffer full
            case UART_BUFFER_FULL:
                ESP_LOGI(TAG_UART, "ring buffer full");
                // If buffer full happened, you should consider encreasing your buffer size
                // As an example, we directly flush the rx buffer here in order to read more data.
                uart_flush_input(UART_NUM_0);
                xQueueReset(uart1_queue);
                break;
            // Event of UART RX break detected
            case UART_BREAK:
                ESP_LOGI(TAG_UART, "uart rx break");
                break;
            // Event of UART parity check error
            case UART_PARITY_ERR:
                ESP_LOGI(TAG_UART, "uart parity error");
                break;
            // Event of UART frame error
            case UART_FRAME_ERR:
                ESP_LOGI(TAG_UART, "uart frame error");
                break;
            // UART_PATTERN_DET
            case UART_PATTERN_DET:
                uart_get_buffered_data_len(UART_NUM_0, &buffered_size);
                int pos = uart_pattern_pop_pos(UART_NUM_0);
                ESP_LOGI(TAG_UART, "[UART PATTERN DETECTED] pos: %d, buffered size: %d", pos, buffered_size);
                break;
            // Others
            default:
                ESP_LOGI(TAG_UART, "uart event type: %d", event.type);
                break;
            }
        }

        else
        {
            printf("No events triggeret from uart!!\n");
        }
    }
    free(dtmp);
    dtmp = NULL;
    vTaskDelete(NULL);
}

//................................................ Tasks..............................................................//

static void mqtt_task(char *data_received, int data_length)
{

    printf("length: %d", sizeof(data_received));
    int isPublished = esp_mqtt_client_publish(client, mqtt_data->pub_topic, data_received, data_length, 1, 0);
    printf("isPublished= %d\n", isPublished);

}

void app_main(void)
{

    // Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    wifi_init_sta();
    vTaskDelay(3000 / portTICK_PERIOD_MS); // wait
    uart_init();
    start_webserver();
    xTaskCreate(uart_event_task, "uart_event_task", 4 * 1024, NULL, 4, &receive_task_handle); // higher priroity
}
