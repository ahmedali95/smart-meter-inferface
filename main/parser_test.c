#include "SMLParser.h"
#include <stdlib.h>
#include <assert.h>
size_t ReadHexFile(FILE *inf, unsigned char *dest)
{
  size_t count = 0;
  int n;
  if (dest == NULL) {
    unsigned char OneByte;
    while ((n = fscanf(inf, "%hhx", &OneByte)) == 1 ) {
      count++;
    }
  }
  else {
    while ((n = fscanf(inf, "%hhx", dest)) == 1 ) {
      dest++;
    }
  }
  if (n != -1) {
    ;  // handle syntax error
  }
  return count;
}
int main() {
  FILE *inf = fopen("test_bytes.txt", "rt");
  size_t n = ReadHexFile(inf, NULL);
  rewind(inf);
  unsigned char *hex = malloc(n);
  // while (i < 5)
  //  {
  //     int no = fscanf(inf, "%hhx", hex);
  //     char read = *hex;
  //     printf("foudn value: %c\n",*hex);

  //     hex++;
  //     i++;
  //   }
  ReadHexFile(inf, hex);
  //parse_data(hex, n);
  fclose(inf);
  free(hex);
  return 0;
 }
